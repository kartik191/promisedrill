const fs = require('fs');


function createDirectory(dir){
    return new Promise((resolve, reject) => {
        fs.mkdir(dir, err => {
            if(err){
                reject(err);
            }
            else{
                resolve();
            }
        });
    });
}

function writeFile(path, data){
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, err => {
            if(err){
                reject(err);
            }
            else{
                resolve();
            }
        });
    });
}

function readdir(dir){
    return new Promise((resolve, reject) => {
        fs.readdir(dir, (err, data) => {
            if(err){
                reject(err);
            }
            else{
                resolve(data);
            }
        });
    });
}

function removeFiles(dir, data){
    return new Promise((resolve, reject) => {
        data.forEach((file, index, array) => {
            fs.unlink(dir + file, (err) => {
                if(err){
                    reject(err);
                }
                if(index === array.length - 1){
                    resolve();
                }
            });
        });
    });
}

function problem1(){
    let data = 'random text';
    const dir = `${__dirname}/json/`;   
    
    createDirectory(dir)
        .then(() => writeFile(`${dir}file1.json`, data))
        .then(() => writeFile(`${dir}file2.json`, data))
        .then(() => writeFile(`${dir}file3.json`, data))
        .then(() => readdir(dir))
        .then((data) => removeFiles(dir, data))
        .catch((err) => console.error(err));
}


module.exports = problem1;