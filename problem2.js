const fs = require('fs');


function readFile(path){
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf-8', (err, data) => {
            if(err){
                reject(err);
            }
            else{
                resolve(data);
            }
        });
    });
}

function writeFile(path, data){
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, err => {
            if(err){
                reject(err);
            }
            else{
                resolve();
            }
        });
    });
}

function appendFile(path, data){
    return new Promise((resolve, reject) => {
        fs.appendFile(path, data, err => {
            if(err){
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}
 
function removeFiles(dir, data){
    return new Promise((resolve, reject) => {
        data.forEach((file, i, array) => {
            fs.unlink(dir + file, (err) => {
                if(err){
                    reject(err);
                }
                else if(i == array.length - 1){ 
                    resolve();
                }
            });
        }); 
    });
}

function problem2(){
    const dir = `${__dirname}/`;
    const lipsumPath = `${dir}lipsum.txt`;
    const filenamesPath = `${dir}filenames.txt`;
    const uppercasePath = `${dir}uppercase.txt`;
    const lowercasePath = `${dir}lowercase.txt`;
    const sortPath = `${dir}sort.txt`;

    readFile(lipsumPath)
        .then(data => writeFile(uppercasePath, data.toUpperCase()))
        .then(() => appendFile(filenamesPath, 'uppercase.txt\n'))
        .then(() => readFile(uppercasePath))
        .then(data => writeFile(lowercasePath, data.toLowerCase().replaceAll('. ','.\n')))
        .then(() => appendFile(filenamesPath, 'lowercase.txt\n'))
        .then(() => readFile(lowercasePath))
        .then(data => writeFile(sortPath, data.split('\n').sort().join('\n')))
        .then(() => appendFile(filenamesPath, 'sort.txt\n'))
        .then(() => readFile(filenamesPath))
        .then(data => removeFiles(dir, data.split('\n').slice(0, -1)))
        .catch(err => console.log(err));
}


module.exports = problem2;